package com.seleniumexpress.sm.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.seleniumexpress.sm.api.Student;
import com.seleniumexpress.sm.rowmapper.StudentRowMapper;


@Repository
public class StudentDAOImpl implements StudentDAO {

	@Autowired
	 JdbcTemplate jdbcTemplate;
	
	
	@Override
	public List<Student> loadStudents() {
		
		String sql = "select * from students;";
		
		List<Student> theListOfStudent = jdbcTemplate.query(sql, new StudentRowMapper());
		
		
		return theListOfStudent;
	}


	@Override
	public void saveStudent(Student student) {
		Object[] sqlParameters = { student.getContry(),student.getMobile(),student.getName()};
		String sql = "insert into students(contry,mobile,name) values(?,?,?)";
		
		jdbcTemplate.update(sql, sqlParameters);
		
		System.out.println("1 record update.....");
	
	}


	@Override
	public Student getStudent(int id) {
		
		String sql = "SELECT * FROM students WHERE id=?";
		
		Student student = jdbcTemplate.queryForObject(sql,new StudentRowMapper() , id);
		
		return student;
	}


	@Override
	public void update(Student student) {
		
		String sql ="UPDATE STUDENTS SET name=?, mobile=?, contry=? WHERE id=?";
		
		jdbcTemplate.update(sql, student.getName(),student.getMobile(),student.getContry(),student.getId());
		System.out.println("1 record updated.....");
	}


	@Override
	public void deletestudent(int id) {
		String sql = "delete from students where id=?";
		jdbcTemplate.update(sql, id );
		
	}

}
