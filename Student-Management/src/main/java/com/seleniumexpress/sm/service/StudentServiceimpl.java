package com.seleniumexpress.sm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seleniumexpress.sm.DAO.StudentDAO;
import com.seleniumexpress.sm.api.Student;

@Service
public class StudentServiceimpl implements StudentService{
	@Autowired
	private StudentDAO studentDAO;
	
	@Override
	public List<Student> loadStudent() {
		
		List<Student> studentList =studentDAO.loadStudents();
		
		
		return studentList;
	}

	@Override
	public void saveStudent(Student student) {
	
		if(student.getContry().equals("uk")) {
			System.out.println("mail sent to : "+ student.getName());
		}
		
		studentDAO.saveStudent(student);
		
	}

	@Override
	public Student getStudent(int id) {
		Student student = studentDAO.getStudent(id);
		
		return student;
	}

	@Override
	public void update(Student student) {
		studentDAO.update(student);
		
	}

	@Override
	public void deleteStudent(int id) {
	
		studentDAO.deletestudent(id);
		
	}

}
