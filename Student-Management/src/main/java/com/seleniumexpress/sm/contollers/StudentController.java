package com.seleniumexpress.sm.contollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seleniumexpress.sm.DAO.StudentDAO;
import com.seleniumexpress.sm.api.Student;
import com.seleniumexpress.sm.service.StudentService;

@Controller
public class StudentController {
	
	// = new StudentDAOImpl() ;
	@Autowired
	private StudentService studentService ;
	
	@GetMapping("/showStudent")
	public String showStudentList(Model model) {

		List<Student> studentList = studentService.loadStudent();

		//for (Student tStudent : studentList) {
			//System.out.println(tStudent);
		//}

		model.addAttribute("students", studentList);

		return "student-list";
	}

	@GetMapping("/showaddStudent")
	public String showaddStudent(Model model) {

		model.addAttribute("student", new Student());

		return "add-student";
	}

	
	@PostMapping( path = "/save-student")
	public String showsaveStudent(Student student) {
		System.out.println(student);
		
		
		// if user have id         -> do a update 
		// id user does't have id  -> do a insert 
		
		if(student.getId() == 0)
			studentService.saveStudent(student);
		else
			studentService.update(student);
		
	//	studentService.saveStudent(student);
		
		return "redirect:/showStudent";
		
	}
	
	@GetMapping("/updateStudent")
	public String updateStudent(@RequestParam("userId") int id,Model model ) {

		Student mystudent = studentService.getStudent(id);
		
		model.addAttribute("student", mystudent);
	
		return "add-student";
	}




	@GetMapping("/deleteStudent")
	public String deleteStudent(@RequestParam("userId") int id) {

	
		studentService.deleteStudent(id);
		
		
		return "redirect:/showStudent";
	}

}

