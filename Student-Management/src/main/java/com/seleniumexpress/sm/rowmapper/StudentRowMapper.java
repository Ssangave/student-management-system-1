package com.seleniumexpress.sm.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.seleniumexpress.sm.api.Student;

public class StudentRowMapper implements RowMapper<Student>{

	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Student student = new Student();
		
		student.setId(rs.getInt(1));
		student.setName(rs.getString(2));
		student.setMobile(rs.getLong(3));
		student.setContry(rs.getString(4));
		return student;
	}

}
