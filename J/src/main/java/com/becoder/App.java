package com.becoder;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.Dao.Dao;
import com.entites.Student;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        ApplicationContext context = new ClassPathXmlApplicationContext("com/becoder/congi.xml");
        Dao d = context.getBean("st",Dao.class);
    
        Student s = new Student();
        s.setName("sso");
        s.setId(982);
        s.setCity("sastur");
        
        int i = d.insert(s);
        System.out.println(i);
    
    }
}
