package com.Dao;

import org.springframework.jdbc.core.JdbcTemplate;

import com.entites.Student;

public class DaoImpl implements Dao{

	private JdbcTemplate jdbcTemplate;

	
	public int insert(Student student) {

		String sql = "insert into student(id,name,city) values(?,?,?)";
		
		int i = jdbcTemplate.update(sql,student.getId(),student.getName(),student.getCity());
		
		return i;
	}


	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
